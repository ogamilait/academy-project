﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Test2.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class MovieDBContext : DbContext
    {
        //public MovieDBContext(): base("DefaultConnection")
        //{ }

        public DbSet<Movie> Movies { get; set; }

    }
}