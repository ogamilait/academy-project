﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test2.Models;

namespace Test2.Controllers
{
    public class HomeController : Controller
    {
        private MovieDBContext db = new MovieDBContext();
        public ActionResult Index()
        {
            ViewBag.test = "test date";
            var film = new Movie()
            {
                Name = "Dota 2",
                Description = "team Ukraine vs F5"
            };

            db.Movies.Add(film);
            db.SaveChanges();

            var res = db.Movies.ToList();

            //db.Movies.Add(new Movi() { Price = 111});
            return View("CustomView");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}